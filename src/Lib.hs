module Lib where

mySort :: [Integer] -> [Integer]
mySort [] = []
mySort (a : as) =
  filter (< a) (mySort as) ++ [a] ++ filter (>= a) (mySort as)

-- $> hspecProgress spec
