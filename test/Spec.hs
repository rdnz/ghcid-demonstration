{-# options_ghc
  -Wno-unused-top-binds
  -Wno-name-shadowing
#-}

import Lib

import Test.Hspec
import Test.Hspec.Runner
  (runSpec, defaultConfig, evaluateSummary, configFormatter)
import Test.Hspec.Formatters (progress)

spec :: Spec
spec = do
  describe "mySort" $ do
    it "sorts [2,1]" $ mySort [2,1] `shouldBe` [1,2]
    it "sorts [2,1,3,5,6,2]" $
      mySort [2,1,3,5,6,2] `shouldBe` [1,2,2,3,5,6]

main :: IO ()
main = hspec spec

hspecProgress :: Spec -> IO ()
hspecProgress spec =
  evaluateSummary
    =<< runSpec spec (defaultConfig {configFormatter = Just progress})

-- $> hspecProgress spec
